# **Welcome to the Mechanics simulator**

 1. Rock mechanics simulation using analytical modelling can be found in the branch [Rocks_analytical_modelling](https://gitlab.tudelft.nl/ADMIRE_Public/mechanics/-/tree/Rocks_analytical_modelling/)

 2. FEM based simulation of rocks can be found in the branch [3DFEM_Mechanics_simulation](https://gitlab.tudelft.nl/ADMIRE_Public/mechanics/-/tree/3DFEM_Mechanics_simulation/src)

 3. Salt cavern based creep simulation can be found in [Salt_cavern2D_simulator](https://gitlab.tudelft.nl/ADMIRE_Public/mechanics/-/edit/Salt_cavern2D_simulator)

 Please read the respective readme files for further understanding.
